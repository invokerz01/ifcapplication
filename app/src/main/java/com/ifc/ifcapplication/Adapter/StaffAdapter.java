package com.ifc.ifcapplication.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ifc.ifcapplication.Model.User;
import com.ifc.ifcapplication.R;

import java.util.ArrayList;

/**
 * Created by TiNkeR-Z on 1/31/2018.
 */

public class StaffAdapter extends ArrayAdapter<User> implements Filterable {
    private Context context;
    private ArrayList<User> staffList;
    private ArrayList<User> filterList;

    public StaffAdapter(Context context, ArrayList<User> staffValues) {
        super(context, R.layout.staff_view, staffValues);
        this.context = context;
        this.staffList = staffValues;
        this.filterList = staffValues;
    }

    private static class ViewHolder {
        TextView txtName;
        TextView txtLastLogin;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        User staff = getItem(position);
        ViewHolder viewHolder;
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = new View(context);
            // get layout from mainstaff.xml
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.staff_view, parent, false);

            // set value into textview
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.tv_staff_name);
            viewHolder.txtLastLogin = (TextView) convertView.findViewById(R.id.tv_last_login);

            result=convertView;

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            convertView = convertView;
        }

        viewHolder.txtName.setText((position+1)+". "+staff.getName());
        if(staff.getLastLogin().equals("null")){
            staff.setLastLogin("never");
        }
        viewHolder.txtLastLogin.setText("Last Login on: "+staff.getLastLogin());

        return convertView;
    }
    private int lastPosition = -1;

    @Override
    public int getCount() {
        return staffList.size();
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public User getItem(int position) {
        return staffList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return staffList.indexOf(getItem(position));
    }


}