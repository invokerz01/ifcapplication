package com.ifc.ifcapplication.Adapter;


import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ifc.ifcapplication.Activity.MainActivity;
import com.ifc.ifcapplication.Model.Table;
import com.ifc.ifcapplication.R;

import java.util.ArrayList;
import java.util.Objects;

public class TableAdapter extends ArrayAdapter<Table> implements Filterable {
    private Context context;
    private ArrayList<Table> tableList;
    private ArrayList<Table> filterList;

    public TableAdapter(Context context, ArrayList<Table> menuValues) {
        super(context, R.layout.table_item_view, menuValues);
        this.context = context;
        this.tableList = menuValues;
        this.filterList = menuValues;
    }

    private static class ViewHolder {
        TextView txtName;
        TextView txtStatus;
        CardView cvTable;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Table table = getItem(position);
        ViewHolder viewHolder;
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = new View(context);
            // get layout from mainmenu.xml
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.table_item_view, parent, false);

            // set value into textview
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.tv_table_name);
            viewHolder.txtStatus = (TextView) convertView.findViewById(R.id.tv_table_status);
            viewHolder.cvTable = (CardView) convertView.findViewById(R.id.cv_table);

            result=convertView;

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            convertView = convertView;
        }

        viewHolder.txtName.setText(table.getName());
        viewHolder.txtStatus.setText(table.getStatus());
        if(Objects.equals(table.getStatus(), "Empty")){
            if(context instanceof MainActivity){
                String where = (String) ((MainActivity)context).getSupportActionBar().getTitle();
                if(Objects.equals(where, "Cash Register")){
                    viewHolder.cvTable.setBackgroundColor(Color.parseColor("#52B3D9"));
                }else{
                    viewHolder.cvTable.setBackgroundColor(Color.parseColor("#87D37C"));
                }
            }
        }else{
            viewHolder.cvTable.setBackgroundColor(Color.parseColor("#EC644B"));
        }

        return convertView;
    }
    private int lastPosition = -1;

    @Override
    public int getCount() {
        return tableList.size();
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public Table getItem(int position) {
        return tableList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return tableList.indexOf(getItem(position));
    }


}
