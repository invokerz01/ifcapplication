package com.ifc.ifcapplication.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ifc.ifcapplication.Activity.TableOrderActivity;
import com.ifc.ifcapplication.Model.Menu;
import com.ifc.ifcapplication.Model.Order;
import com.ifc.ifcapplication.Model.Table;
import com.ifc.ifcapplication.R;
import com.ifc.ifcapplication.TableOrderDBHelper;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import static com.ifc.ifcapplication.R.layout.order_item;

public class OrderAdapter extends ArrayAdapter<Menu> implements View.OnClickListener{
    TableOrderDBHelper db;
    private Context context;
    private ArrayList<Menu> menuValues;
    private Table table;


    public OrderAdapter(ArrayList<Menu> data, Context context,Table table) {
        super(context, order_item, data);
        this.menuValues = data;
        this.context=context;
        this.table = table;

    }



    public static class ViewHolder {
        TextView tvName;
        TextView tvPrice;
        EditText etQuantity;
        ImageButton btnAdd;
        ImageButton btnMinus;

    }

    @Override
    public void onClick(View v) {
        db = new TableOrderDBHelper(context);
        int position=(Integer) v.getTag();
        Object object= getItem(position);
        Menu menu=(Menu)object;
        boolean d;
        switch (v.getId())
        {
            case R.id.btn_plus:
                d = db.insertMenu(menu,table);
                if(d){
                    //Toast.makeText(context, "I am inserting menu "+menu.hashCode()+" into order "+position, Toast.LENGTH_SHORT).show();
                }else{
                    //Toast.makeText(context, "Not Successful"+position, Toast.LENGTH_SHORT).show();
                }
                if(context instanceof TableOrderActivity){
                    ((TableOrderActivity)context).updateOrder();
                }
                this.notifyDataSetChanged();
                break;
            case R.id.btn_minus:
                d = db.removeSingleMenu(menu);
                if(d){
                    //Toast.makeText(context, "I am removing a menu from order "+position, Toast.LENGTH_SHORT).show();
                }else{
                    menuValues.remove(position);
                    if(menuValues.isEmpty()){
                        TableOrderActivity.txtOrderStatus.setVisibility(View.VISIBLE);
                    }
                    this.notifyDataSetChanged();
                }
                if(context instanceof TableOrderActivity){
                    ((TableOrderActivity)context).updateOrder();
                }
                this.notifyDataSetChanged();
                break;
        }
    }

    private int lastPosition = -1;

    @Override
    public int getCount(){
        return menuValues.size();
    }

    @Override
    public Menu getItem(int pos){
        return menuValues.get(pos);
    }

    @Override
    public long getItemId(int pos){
        return menuValues.indexOf(getItem(pos));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        db = new TableOrderDBHelper(context);
        Menu menuModel = getItem(position);
        Order order = db.getOrder(menuModel.getId());
        double subtotal = 0;
        int quantity = 0;
        if(order.getMenu_id() ==menuModel.getId()){
            subtotal = order.getSubtotal();
            quantity = order.getQuantity();
        }
        // Check if an existing view is being reused, otherwise inflate the view
        final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(order_item, parent, false);

            // set value into textview
            viewHolder.tvName =(TextView) convertView.findViewById(R.id.textView_name);
            viewHolder.tvPrice =(TextView) convertView.findViewById(R.id.textView_price);
            viewHolder.btnAdd =(ImageButton) convertView.findViewById(R.id.btn_plus);
            viewHolder.btnMinus =(ImageButton) convertView.findViewById(R.id.btn_minus);
            viewHolder.etQuantity = (EditText) convertView.findViewById(R.id.editText_quantity) ;

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        lastPosition = position;

        viewHolder.tvName.setText((position+1)+". "+menuModel.getName());
        viewHolder.btnAdd.setOnClickListener(this);
        viewHolder.btnMinus.setOnClickListener(this);
        viewHolder.btnAdd.setTag(position);
        viewHolder.btnMinus.setTag(position);
        NumberFormat formatter = new DecimalFormat("#0.00");
        viewHolder.tvPrice.setText("Total: RM "+formatter.format(subtotal));
        viewHolder.etQuantity.setText(""+quantity);
        return convertView;
    }
}
