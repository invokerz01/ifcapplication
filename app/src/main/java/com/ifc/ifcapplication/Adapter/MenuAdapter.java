package com.ifc.ifcapplication.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ifc.ifcapplication.Model.Menu;
import com.ifc.ifcapplication.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

public class MenuAdapter extends ArrayAdapter<Menu> implements Filterable {
    private Context context;
    private ArrayList<Menu> menuList;
    private ArrayList<Menu> filterList;
    CustomFilter filter;

    public MenuAdapter(Context context, ArrayList<Menu> menuValues) {
        super(context, R.layout.menu_list_item, menuValues);
        this.context = context;
        this.menuList = menuValues;
        this.filterList = menuValues;
    }

    private static class ViewHolder {
        TextView txtName;
        TextView txtPrice;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Menu menu = getItem(position);
        ViewHolder viewHolder;
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = new View(context);
            // get layout from mainmenu.xml
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.menu_list_item, parent, false);

            // set value into textview
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.txt_menu_name);
            viewHolder.txtPrice = (TextView) convertView.findViewById(R.id.txt_menu_price);

            result=convertView;

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            convertView = convertView;
        }

        viewHolder.txtName.setText((position+1)+". "+menu.getName());
        NumberFormat formatter = new DecimalFormat("#0.00");
        viewHolder.txtPrice.setText("RM "+formatter.format(menu.getPrice()));

        return convertView;
    }
    private int lastPosition = -1;

    @Override
    public int getCount() {
        return menuList.size();
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public Menu getItem(int position) {
        return menuList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return menuList.indexOf(getItem(position));
    }

    @Override
    public Filter getFilter() {
        if (filter == null){
            filter = new CustomFilter();
        }
        return filter;
    }

    private class CustomFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if(constraint != null && constraint.length()>1){
                constraint = constraint.toString().toUpperCase();
                ArrayList<Menu> filters = new ArrayList<>();

                for(int i=0;i<filterList.size();i++){
                    if( filterList.get(i).getName().toUpperCase().contains(constraint)) {

                        Menu m = new Menu(filterList.get(i).getId(), filterList.get(i).getName(), filterList.get(i).getPrice(), filterList.get(i).getType());

                        filters.add(m);
                    }
                }
                results.count = filters.size();
                results.values = filters;
            }
            else{
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            menuList =  (ArrayList<Menu>) results.values;
            notifyDataSetChanged();
        }
    }


}

