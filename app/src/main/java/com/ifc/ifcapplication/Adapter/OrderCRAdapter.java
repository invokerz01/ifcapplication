package com.ifc.ifcapplication.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ifc.ifcapplication.Activity.CashRegisterActivity;
import com.ifc.ifcapplication.Model.Menu;
import com.ifc.ifcapplication.Model.Order;
import com.ifc.ifcapplication.Model.Table;
import com.ifc.ifcapplication.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;

import static com.ifc.ifcapplication.R.layout.order_item;

public class OrderCRAdapter extends ArrayAdapter<Menu> {
    private Context context;
    private ArrayList<Order> orderValues;
    private ArrayList<Menu> menuValues;
    private Table table;


    public OrderCRAdapter(ArrayList<Menu> data,ArrayList<Order> dataOrder, Context context,Table table) {
        super(context, order_item, data);
        this.orderValues = dataOrder;
        this.menuValues = data;
        this.context=context;
        this.table = table;

    }



    public static class ViewHolder {
        TextView tvName;
        TextView tvPrice;
        EditText etQuantity;
        ImageButton btnAdd;
        ImageButton btnMinus;

    }


    private int lastPosition = -1;

    @Override
    public int getCount(){
        return menuValues.size();
    }

    @Override
    public Menu getItem(int pos){
        return menuValues.get(pos);
    }

    @Override
    public long getItemId(int pos){
        return menuValues.indexOf(getItem(pos));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Menu menuModel = getItem(position);
        double subtotal = 0;
        int quantity = 0;
        for (Iterator<Order> i = orderValues.iterator(); i.hasNext();) {
            Order item = i.next();
            if(item.getMenu_id() == menuModel.getId()){
                subtotal = item.getSubtotal();
                quantity = item.getQuantity();
            }
        }


        // Check if an existing view is being reused, otherwise inflate the view
        final ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(order_item, parent, false);

            // set value into textview
            viewHolder.tvName =(TextView) convertView.findViewById(R.id.textView_name);
            viewHolder.tvPrice =(TextView) convertView.findViewById(R.id.textView_price);
            viewHolder.btnAdd =(ImageButton) convertView.findViewById(R.id.btn_plus);
            viewHolder.btnMinus =(ImageButton) convertView.findViewById(R.id.btn_minus);
            viewHolder.etQuantity = (EditText) convertView.findViewById(R.id.editText_quantity) ;

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        lastPosition = position;

        viewHolder.tvName.setText((position+1)+". "+menuModel.getName());
        NumberFormat formatter = new DecimalFormat("#0.00");
        viewHolder.tvPrice.setText("RM "+formatter.format(subtotal));
        viewHolder.etQuantity.setText(""+quantity);
        if(context instanceof CashRegisterActivity){
            viewHolder.btnAdd.setVisibility(View.INVISIBLE);
            viewHolder.btnMinus.setVisibility(View.INVISIBLE);
        }
        return convertView;
    }
}