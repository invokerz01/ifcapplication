package com.ifc.ifcapplication.Adapter;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ifc.ifcapplication.Model.Sales;
import com.ifc.ifcapplication.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by TiNkeR-Z on 2/1/2018.
 */

public class SalesAdapter extends ArrayAdapter<Sales> implements Filterable {
    private Context context;
    private ArrayList<Sales> salesList;
    private ArrayList<Sales> filterList;

    public SalesAdapter(Context context, ArrayList<Sales> salesValues) {
        super(context, R.layout.sales_item_view, salesValues);
        this.context = context;
        this.salesList = salesValues;
        this.filterList = salesValues;
    }

    private static class ViewHolder {
        TextView txtName;
        TextView txtDate;
        TextView txtPrice;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Sales sales = getItem(position);
        ViewHolder viewHolder;
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = new View(context);
            // get layout from mainsales.xml
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.sales_item_view, parent, false);

            // set value into textview
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.txtPrice = (TextView) convertView.findViewById(R.id.tv_total);
            viewHolder.txtDate = (TextView) convertView.findViewById(R.id.tv_date);

            result=convertView;

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            convertView = convertView;
        }

        if(Objects.equals(sales.getMode().toLowerCase(), "daily")){
            viewHolder.txtName.setText(sales.getDate());
            viewHolder.txtDate.setText(sales.getDay());
        }else if (Objects.equals(sales.getMode().toLowerCase(), "monthly")){
            viewHolder.txtName.setText(sales.getMonth());
            viewHolder.txtDate.setVisibility(View.INVISIBLE);
        }else if (Objects.equals(sales.getMode().toLowerCase(), "yearly")){
            viewHolder.txtName.setText(sales.getYear());
            viewHolder.txtDate.setVisibility(View.INVISIBLE);
        }
        NumberFormat formatter = new DecimalFormat("#0.00");
        viewHolder.txtPrice.setText("RM "+formatter.format(sales.getTotal()));

        return convertView;
    }
    private int lastPosition = -1;

    @Override
    public int getCount() {
        return salesList.size();
    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public Sales getItem(int position) {
        return salesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return salesList.indexOf(getItem(position));
    }


}
