package com.ifc.ifcapplication;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ifc.ifcapplication.Model.Menu;
import com.ifc.ifcapplication.Model.Order;
import com.ifc.ifcapplication.Model.Table;

import java.util.ArrayList;


public class TableOrderDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "customer_order.db";
    private static final String ORDER_TABLE_NAME = "customer_order";
    private static final String ORDER_ID = "id";
    private static final String ORDER_TABLE_ID = "table_id";
    private static final String ORDER_MENU_ID = "menu_id";
    private static final String ORDER_MENU_QUANTITY = "quantity";
    private static final String ORDER_SUBTOTAL = "subtotal";


    public TableOrderDBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE customer_order (id INTEGER PRIMARY KEY, table_id INTEGER,menu_id INTEGER,quantity INTEGER,subtotal REAL)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS customer_order");
        onCreate(db);
    }

    public boolean insertMenu(Menu Menu,Table table) {
        SQLiteDatabase db = this.getWritableDatabase();
        if(MenuExists(Menu.getId())){
            Order tempOrder = getOrder(Menu.getId());
            ContentValues contentValues = new ContentValues();
            contentValues.put(ORDER_TABLE_ID, table.getId());
            contentValues.put(ORDER_MENU_ID, Menu.getId());
            contentValues.put(ORDER_MENU_QUANTITY, tempOrder.getQuantity()+1);
            contentValues.put(ORDER_SUBTOTAL, tempOrder.getSubtotal()+Menu.getPrice());
            db.update(ORDER_TABLE_NAME, contentValues, "menu_id="+Menu.getId(), null);
            return false;
        }else{
            ContentValues contentValues = new ContentValues();
            contentValues.put(ORDER_TABLE_ID, table.getId());
            contentValues.put(ORDER_MENU_ID, Menu.getId());
            contentValues.put(ORDER_MENU_QUANTITY, 1);
            contentValues.put(ORDER_SUBTOTAL, Menu.getPrice());
            db.insert(ORDER_TABLE_NAME, null, contentValues);
            return true;
        }
    }

    public boolean removeSingleMenu(Menu Menu) {
        SQLiteDatabase db = this.getWritableDatabase();
        if(MenuExists(Menu.getId())){
            Order tempOrder = getOrder(Menu.getId());
            if(tempOrder.getQuantity()>1){
                ContentValues contentValues = new ContentValues();
                contentValues.put(ORDER_MENU_ID, Menu.getId());
                contentValues.put(ORDER_MENU_QUANTITY, tempOrder.getQuantity()-1);
                contentValues.put(ORDER_SUBTOTAL, tempOrder.getSubtotal()-Menu.getPrice());
                db.update(ORDER_TABLE_NAME, contentValues, "menu_id="+Menu.getId(), null);
                return true;
            }else if(tempOrder.getQuantity() == 1){
                db.delete(ORDER_TABLE_NAME,"menu_id="+Menu.getId(),null);
                return false;
            }
        }
        return true;
    }

    public boolean removeMenu(Menu Menu) {
        SQLiteDatabase db = this.getWritableDatabase();
        if(MenuExists(Menu.getId())){
            Order tempOrder = getOrder(Menu.getId());
            db.delete(ORDER_TABLE_NAME,"menu_id="+Menu.getId(),null);
            return false;
        }
        return true;
    }


    public boolean MenuExists(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * from " + ORDER_TABLE_NAME + " where " + ORDER_MENU_ID + " = " + id;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public boolean deleteAllFromOrder() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(ORDER_TABLE_NAME,null,null);
        return true;
    }



    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from cart where id="+id+"", null );
        return res;
    }

    // check cart db by Menu id
    public Order getOrder(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(ORDER_TABLE_NAME, new String[] { ORDER_ID ,
                        ORDER_MENU_ID,ORDER_TABLE_ID,ORDER_MENU_QUANTITY,ORDER_SUBTOTAL}, ORDER_MENU_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Order order = new Order(Integer.parseInt(cursor.getString(0)),Integer.parseInt(cursor.getString(1)),Integer.parseInt(cursor.getString(2)),Integer.parseInt(cursor.getString(3)),Double.parseDouble(cursor.getString(4)));

        return order;
    }

    public ArrayList<Order> getAllOrder() {
        ArrayList<Order> orderList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + ORDER_TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Order cart = new Order(Integer.parseInt(cursor.getString(0)),Integer.parseInt(cursor.getString(2)),Integer.parseInt(cursor.getString(1)),Integer.parseInt(cursor.getString(3)),Double.parseDouble(cursor.getString(4)));
                // Adding contact to list
                orderList.add(cart);
            } while (cursor.moveToNext());
        }

        // return contact list
        db.close();
        return orderList;
    }


    // Getting cart count
    public int getOrderCount() {
        int count = 0;
        String countQuery = "SELECT  * FROM " + ORDER_TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

}