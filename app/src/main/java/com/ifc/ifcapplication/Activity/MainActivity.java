package com.ifc.ifcapplication.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.ifc.ifcapplication.Fragment.AboutFragment;
import com.ifc.ifcapplication.Fragment.CashRegisterFragment;
import com.ifc.ifcapplication.Fragment.HelpFragment;
import com.ifc.ifcapplication.Fragment.ManageMenuFragment;
import com.ifc.ifcapplication.Fragment.ManageStaffFragment;
import com.ifc.ifcapplication.Fragment.MenuListFragment;
import com.ifc.ifcapplication.Fragment.SalesReportFragment;
import com.ifc.ifcapplication.Fragment.TablesFragment;
import com.ifc.ifcapplication.Model.User;
import com.ifc.ifcapplication.R;
import com.ifc.ifcapplication.TableOrderDBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,TablesFragment.OnFragmentInteractionListener,AboutFragment.OnFragmentInteractionListener,
        CashRegisterFragment.OnFragmentInteractionListener,HelpFragment.OnFragmentInteractionListener,ManageMenuFragment.OnFragmentInteractionListener,
        ManageStaffFragment.OnFragmentInteractionListener,MenuListFragment.OnFragmentInteractionListener,SalesReportFragment.OnFragmentInteractionListener{
    int type = 0;
    DrawerLayout drawer;
    NavigationView navigationView;
    TextView txtUser,txtType;
    public User loggedUser;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            type = extras.getInt("usertype");
            // For Debugging purpose
            if(type == 1)
                Toast.makeText(this, "Login as Manager", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "Login as Staff", Toast.LENGTH_SHORT).show();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("RKM Mobile Ordering");


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header= navigationView.getHeaderView(0);
        txtUser = (TextView)header.findViewById(R.id.txt_name);
        txtType = (TextView)header.findViewById(R.id.txt_type);
        db = new TableOrderDBHelper(this).getWritableDatabase();
        // Limiting navigation view for staffs and manager
        switch(type){
            case 1:
                navigationView.getMenu().findItem(R.id.nav_manager).setVisible(true);
                txtType.setText("Manager");
                break;
            case 2:
                navigationView.getMenu().findItem(R.id.nav_manager).setVisible(false);
                txtType.setText("Staff");
                break;

        }
        changeToTablesFragment();
        getUserData();
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
        } else {
            doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        Class fragmentClass = null;
        FragmentManager fragmentManager = getSupportFragmentManager();
        String title = "";
        if(id != R.id.nav_logout){
            if (id == R.id.nav_table) {
                fragmentClass = TablesFragment.class;
                title = "Tables";
            } else if (id == R.id.nav_menu_list) {
                fragmentClass = MenuListFragment.class;
                title = "Menu List";
            }
            /*else if (id == R.id.nav_about) {
                fragmentClass = AboutFragment.class;
                title = "About";
            } else if (id == R.id.nav_help) {
                fragmentClass = HelpFragment.class;
                title = "Help";
            }
            */
            else if (id == R.id.nav_cash_register) {
                fragmentClass = CashRegisterFragment.class;
                title = "Cash Register";
            } else if (id == R.id.nav_staff) {
                fragmentClass = ManageStaffFragment.class;
                title = "Manage Staff";
            }else if (id == R.id.nav_manage_menu) {
                fragmentClass = ManageMenuFragment.class;
                title = "Manage Menu";
            }
            else if (id == R.id.nav_sales_report) {
                fragmentClass = SalesReportFragment.class;
                title = "Sales Report";
            }
            try {
                fragment = (Fragment) fragmentClass.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("title", title);
                fragment.setArguments(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }

            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        }else {
            logoutConfirmation();
        }
        return true;
    }

    public void changeToTablesFragment(){
        Fragment fragment = null;
        navigationView.setCheckedItem(R.id.nav_table);
        Bundle bundle = new Bundle();
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
        Class fragmentClass = TablesFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
            bundle.putString("title", "Tables");
            fragment.setArguments(bundle);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        tx.replace(R.id.flContent, fragment);
        tx.commit();
    }

    void logout(){
        Intent i = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(i);
        this.finish();
    }

    void logoutConfirmation(){

        new AlertDialog.Builder(this)
                .setTitle("Logout Confirmation")
                .setMessage("You Sure Want to Logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        logout();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .show();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
    void getUserData(){
        SharedPreferences sharedPref = getSharedPreferences("userdetail", Context.MODE_PRIVATE);
        int id = sharedPref.getInt("user_id",0);
        OkHttpClient client = new OkHttpClient();
        String url = LoginActivity.mainUrl+"userControl.php?user_id="+id;

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONArray dataJsonArr = null;
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");

                    if(Objects.equals(message, "OK")){
                        dataJsonArr = json.getJSONArray("user");
                        int id = 0;
                        String name = "";
                        String password ="";
                        String username = "";
                        String ic_passport = "";
                        String citizenship = "";
                        String lastLogin = "";
                        String userType= "";
                        for (int i = 0; i < dataJsonArr.length(); i++) {
                            JSONObject c = dataJsonArr.getJSONObject(i);
                            // Storing each json item in variable
                            id = c.getInt("id");
                            name = c.getString("name");
                            password = c.getString("password");
                            username = c.getString("username");
                            ic_passport = c.getString("ic_passport");
                            citizenship = c.getString("citizenship");
                            lastLogin = c.getString("last_login");
                            userType = c.getString("user_type");
                            loggedUser = new User(id, name, ic_passport, citizenship, lastLogin, username, password, userType);

                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                txtUser.setText("Login as: "+loggedUser.getName());
                            }
                        });
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, ""+message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
