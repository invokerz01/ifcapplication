package com.ifc.ifcapplication.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.ifc.ifcapplication.Adapter.MenuAdapter;
import com.ifc.ifcapplication.Model.Menu;
import com.ifc.ifcapplication.Model.Table;
import com.ifc.ifcapplication.R;
import com.ifc.ifcapplication.TableOrderDBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ChooseMenuActivity extends AppCompatActivity implements View.OnClickListener {
    TableOrderDBHelper db;
    String type ="";
    ArrayList<Menu> menuList;
    ArrayAdapter<Menu> menuAdapter;
    ListView listViewMenu;
    Button btnAll,btnFood,btnDrink;
    Table table;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_menu);
        db = new TableOrderDBHelper(this);
        btnAll = (Button)findViewById(R.id.btn_all);
        btnFood = (Button)findViewById(R.id.btn_food);
        btnDrink = (Button)findViewById(R.id.btn_drink);
        btnAll.setOnClickListener(this);
        btnFood.setOnClickListener(this);
        btnDrink.setOnClickListener(this);
        listViewMenu = (ListView)findViewById(R.id.listview_menu);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            table = extras.getParcelable("table");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Choose Menu For "+table.getName());
        }
        menuList = new ArrayList<>();
        getMenuList("all");
        menuAdapter = new MenuAdapter(ChooseMenuActivity.this,menuList);
        listViewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Menu menu = menuAdapter.getItem(position);
                boolean d = db.insertMenu(menu,table);
                if(d){
                    Toast.makeText(ChooseMenuActivity.this, "Added "+menu.getName()+" into order", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(ChooseMenuActivity.this, "Updated quantity "+menu.getName()+" in order", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ChooseMenuActivity.this,TableOrderActivity.class);
        i.putExtra("table",table);
        startActivity(i);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        db.close();
        db = null;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_all:
                type ="all";
                getMenuList(type);
                break;
            case R.id.btn_food:
                type ="food";
                getMenuList(type);
                break;
            case R.id.btn_drink:
                type ="drink";
                getMenuList(type);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.menuSearch);
        MenuItem itemAdd = menu.findItem(R.id.action_add);
        itemAdd.setVisible(false);
        SearchView searchView = (SearchView)item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                menuAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }



    void getMenuList(String type){
        menuList.clear();
        OkHttpClient client = new OkHttpClient();
        String url = LoginActivity.mainUrl+"getMenuList.php?type="+type;

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ChooseMenuActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONArray dataJsonArr = null;
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");


                    dataJsonArr = json.getJSONArray("menu");
                    int id = 0;
                    String name = "";
                    double price = 0.0;
                    String type = "";
                    for (int i = 0; i < dataJsonArr.length(); i++) {
                        JSONObject c = dataJsonArr.getJSONObject(i);
                        // Storing each json item in variable
                        id = c.getInt("id");
                        name = c.getString("name");
                        price = c.getDouble("price");
                        type = c.getString("type");
                        menuList.add(new Menu(id,name,price,type));
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listViewMenu.setAdapter(menuAdapter);
                            menuAdapter.notifyDataSetChanged();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
