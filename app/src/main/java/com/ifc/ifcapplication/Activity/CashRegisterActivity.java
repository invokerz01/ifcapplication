package com.ifc.ifcapplication.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ifc.ifcapplication.Adapter.OrderCRAdapter;
import com.ifc.ifcapplication.Model.Menu;
import com.ifc.ifcapplication.Model.Order;
import com.ifc.ifcapplication.Model.Table;
import com.ifc.ifcapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CashRegisterActivity extends AppCompatActivity implements View.OnClickListener{
    double totalPrice = 0,cashGiven = 0;
    Button submitPayment;
    TextView txtTotalPrice,txtOrderStatus;
    ArrayList<Order> orderArrayList;
    ArrayList<Menu> menuArrayList;
    ArrayAdapter<Menu> orderAdapter;
    Table table;
    ListView listViewOrder;
    NumberFormat formatter = new DecimalFormat("#0.00");
    AlertDialog alertReceipt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_register);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            table = extras.getParcelable("table");
            getSupportActionBar().setTitle(table.getName());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        txtTotalPrice = (TextView)findViewById(R.id.textView_total_price);
        submitPayment = (Button)findViewById(R.id.btn_submit_payment);
        submitPayment.setOnClickListener(this);
        txtOrderStatus= (TextView)findViewById(R.id.tv_result);
        listViewOrder = (ListView)findViewById(R.id.listView_order);
        menuArrayList = new ArrayList<>();
        orderArrayList = new ArrayList<>();
        getMenuData();
        orderAdapter= new OrderCRAdapter(menuArrayList,orderArrayList,this,table);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void doPayment(){
        final EditText txtPayment = new EditText(this);
        // Set the default text to a link of the Queen
        txtPayment.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        txtPayment.setHint("Enter amount");

        new AlertDialog.Builder(this)
                .setTitle("Payment for "+ table.getName())
                .setMessage("Total Payment is: RM"+formatter.format(totalPrice))
                .setView(txtPayment)
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if(txtPayment.length() <= 0){
                            Toast.makeText(CashRegisterActivity.this, "Please enter Amount", Toast.LENGTH_SHORT).show();
                        }else{
                            try{
                                cashGiven = Double.parseDouble(txtPayment.getText().toString());
                                if(cashGiven >= totalPrice){
                                    Double change = cashGiven - totalPrice;
                                    Toast.makeText(CashRegisterActivity.this, "RM"+formatter.format(cashGiven), Toast.LENGTH_SHORT).show();
                                    Toast.makeText(CashRegisterActivity.this, "Change is RM"+formatter.format(change), Toast.LENGTH_SHORT).show();
                                    settlePayment(cashGiven,totalPrice,change);
                                }else{
                                    Toast.makeText(CashRegisterActivity.this, "Amount entered is less than RM "+formatter.format(totalPrice)+" !", Toast.LENGTH_SHORT).show();
                                }
                            }
                            catch(Exception e){
                                Toast.makeText(CashRegisterActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .show();
    }

    public void updateLayout(){
        if(menuArrayList.size() > 0){
            final NumberFormat formatter = new DecimalFormat("#0.00");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    listViewOrder.setAdapter(orderAdapter);
                    txtOrderStatus.setVisibility(View.INVISIBLE);
                    txtTotalPrice.setText("Total Price :"+formatter.format(totalPrice));
                }
            });
        }else{
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    submitPayment.setVisibility(View.INVISIBLE);
                    txtTotalPrice.setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    void receiptDialog(Double cashGiven, Double totalPrice, Double change){
        TextView tvTotal,tvAmountPay,tvChange;
        LayoutInflater inflater = getLayoutInflater();
        View addMenuView = inflater.inflate(R.layout.receipt_view, null);
        final ListView lvReceipt = (ListView) addMenuView.findViewById(R.id.lv_receipt);
        Button btnClose = (Button) addMenuView.findViewById(R.id.btn_close);
        tvTotal = (TextView) addMenuView.findViewById(R.id.tv_total_cost);
        tvAmountPay = (TextView) addMenuView.findViewById(R.id.tv_amount_payed);
        tvChange = (TextView) addMenuView.findViewById(R.id.tv_change);

        tvTotal.setText("Total Cost: RM"+formatter.format(totalPrice));
        tvAmountPay.setText("Amount Payed: RM"+formatter.format(cashGiven));
        tvChange.setText("Change: RM"+formatter.format(change));
        lvReceipt.setAdapter(orderAdapter);
        alertReceipt = new AlertDialog.Builder(this)
                .setTitle("Receipt")
                .setView(addMenuView)
                .show();
        btnClose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // I want the dialog to close at this point
                alertReceipt.dismiss();
                onBackPressed();
            }
        });
    }

    void settlePayment(final Double cashGiven, final Double totalPrice, final Double change){
        String url = LoginActivity.mainUrl+"payment.php?amount_pay="+cashGiven+"&amount_total="+totalPrice+"&amount_change="+change+"&table_id="+table.getId();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CashRegisterActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");
                    if(Objects.equals(message, "OK")){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CashRegisterActivity.this, "Payment has been made", Toast.LENGTH_SHORT).show();
                                receiptDialog(cashGiven, totalPrice, change);
                            }
                        });
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CashRegisterActivity.this, ""+message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    void getMenuData(){
        menuArrayList.clear();
        orderArrayList.clear();
        String url = LoginActivity.mainUrl+"getMenuList.php?table_id="+table.getId();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CashRegisterActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONArray dataJsonArr = null;
                    JSONArray menuDetailsJsonArr = null;
                    JSONObject json = new JSONObject(myResponse);
                    // Category Basic Values
                    final String message = json.getString("message");

                    if(Objects.equals(message, "OK")){
                        totalPrice = json.getDouble("total_price");

                        dataJsonArr = json.getJSONArray("menu");
                        menuDetailsJsonArr = json.getJSONArray("menu_details");
                        int id = 0;
                        String name = "";
                        double price = 0.0;
                        String type = "";
                        for (int i = 0; i < dataJsonArr.length(); i++) {
                            JSONObject c = dataJsonArr.getJSONObject(i);
                            // Storing each json item in variable
                            id = c.getInt("id");
                            name = c.getString("name");
                            price = c.getDouble("price");
                            type = c.getString("type");
                            int order_item_id =0;
                            int table_id=0;
                            int menu_id=0;
                            int order_quantity =0;
                            double order_subtotal = 0;
                            menuArrayList.add(new com.ifc.ifcapplication.Model.Menu(id,name,price,type));
                            for (int j = 0; j < menuDetailsJsonArr.length(); j++) {
                                JSONObject b = menuDetailsJsonArr.getJSONObject(j);
                                if (b.getInt("menu_id") == id) {
                                    order_item_id = b.getInt("id");
                                    table_id = table.getId();
                                    menu_id = b.getInt("menu_id");
                                    order_quantity += b.getInt("quantity");
                                }
                                order_subtotal = price*order_quantity;
                            }
                            orderArrayList.add(new Order(order_item_id, menu_id, table_id, order_quantity, order_subtotal));
                        }
                        updateLayout();
                    }else{
                        updateLayout();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CashRegisterActivity.this, ""+message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_submit_payment:
                doPayment();
                break;
        }
    }
}
