package com.ifc.ifcapplication.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ifc.ifcapplication.Adapter.OrderAdapter;
import com.ifc.ifcapplication.Model.Order;
import com.ifc.ifcapplication.Model.Table;
import com.ifc.ifcapplication.R;
import com.ifc.ifcapplication.TableOrderDBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class TableOrderActivity extends AppCompatActivity implements View.OnClickListener{
    Table table;
    ListView listViewCurrent;
    ArrayList<Order> orderList;
    public static TextView txtTotalPrice,txtOrderStatus;
    ArrayList<com.ifc.ifcapplication.Model.Menu> menuArrayList;
    ArrayAdapter<com.ifc.ifcapplication.Model.Menu> orderAdapter;
    TableOrderDBHelper db;
    int totalQuantity;
    String sql = "";
    double totalPrice = 0;
    Button submitOrder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_order);
        submitOrder = (Button) findViewById(R.id.btn_submit_payment);
        submitOrder.setOnClickListener(this);
        db = new TableOrderDBHelper(this);
        orderList = db.getAllOrder();
        menuArrayList = new ArrayList<>();
        txtTotalPrice = (TextView) findViewById(R.id.textView_total_price);
        txtOrderStatus = (TextView) findViewById(R.id.tv_result);
        listViewCurrent = (ListView)findViewById(R.id.listView_current);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            table = extras.getParcelable("table");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(table.getName());
        }

        if(orderList != null && orderList.size() > 0){
            getMenuData();
            updateOrder();
            orderAdapter= new OrderAdapter(menuArrayList,this,table);
        }else{
            txtTotalPrice.setVisibility(View.INVISIBLE);
            txtOrderStatus.setVisibility(View.VISIBLE);
        }
    }

    public void updateOrder(){
        int count = 0;
        totalQuantity = 0;
        totalPrice = 0;
        db = new TableOrderDBHelper(this);
        orderList = db.getAllOrder();
        for (Iterator<Order> i = orderList.iterator(); i.hasNext();) {
            Order item = i.next();
            totalQuantity += item.getQuantity();
            totalPrice += item.getSubtotal();
            count++;
        }
        if(orderList.size() > 0){
            final NumberFormat formatter = new DecimalFormat("#0.00");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txtOrderStatus.setVisibility(View.INVISIBLE);
                    txtTotalPrice.setText("Total Price :"+formatter.format(totalPrice));
                }
            });
        }else{
            txtTotalPrice.setVisibility(View.INVISIBLE);
        }

    }

    void setOrder(){
        int count = 0;
        for (Iterator<Order> i = orderList.iterator(); i.hasNext();) {
            Order item = i.next();
            if(count == 0){
                sql += "id[]="+item.getMenu_id();
            }else if(count>0){
                sql += "&id[]="+item.getMenu_id();
            }
            count++;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        db = new TableOrderDBHelper(this);
        db.deleteAllFromOrder();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_table, menu);
        return super.onCreateOptionsMenu(menu);
    }

    void onOpenMenuActivity(){
        db.close();
        db = null;
        Intent i = new Intent(TableOrderActivity.this,ChooseMenuActivity.class);
        i.putExtra("table",table);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_add:
                onOpenMenuActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void getMenuData(){
        setOrder();
        String id = sql;
        String url = LoginActivity.mainUrl+"getMenuList.php?"+id;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(TableOrderActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONArray dataJsonArr = null;
                    JSONObject json = new JSONObject(myResponse);
                    // Category Basic Values

                    dataJsonArr = json.getJSONArray("menu");
                    int id = 0;
                    String name = "";
                    double price = 0.0;
                    String type = "";
                    for (int i = 0; i < dataJsonArr.length(); i++) {
                        JSONObject c = dataJsonArr.getJSONObject(i);
                        // Storing each json item in variable
                        id = c.getInt("id");
                        name = c.getString("name");
                        price = c.getDouble("price");
                        type = c.getString("type");
                        menuArrayList.add(new com.ifc.ifcapplication.Model.Menu(id,name,price,type));
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listViewCurrent.setAdapter(orderAdapter);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void submitOrder(){
        if(orderList.size() == 0){
            Toast.makeText(this, "Current order is empty", Toast.LENGTH_SHORT).show();
            return;
        }
        String saveSql = "table_id="+table.getId();
        int count = 0;
        for (Iterator<Order> i = orderList.iterator(); i.hasNext();) {
            Order item = i.next();
            saveSql += "&menu_id[]="+item.getMenu_id() + "&quantity[]="+item.getQuantity()+"&price[]="+item.getSubtotal();
            count++;
        }
        String url = LoginActivity.mainUrl+"admin/setOrder.php?"+saveSql;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(TableOrderActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONObject json = new JSONObject(myResponse);
                    String message = json.getString("message");
                    final String orderID = json.getString("order_id");
                    if(Objects.equals(message, "OK")){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(TableOrderActivity.this, "Order has been submitted. Order: "+orderID, Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_submit_payment:
                submitOrder();
                break;
        }
    }
}
