package com.ifc.ifcapplication.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.ifc.ifcapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnLogin;
    RadioGroup rgType;
    EditText editTxUsername,editTxPassword;
    public static String mainUrl;
    ImageView logoView;
    String url;
    FrameLayout flLoading;
    int type = 2; // default type
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mainUrl = "http://192.168.0.132/";
        SharedPreferences linkDetail = getSharedPreferences("link",Context.MODE_PRIVATE);
        String link = linkDetail.getString("link","");
        if(Objects.equals(link, "")){
            SharedPreferences.Editor edit = linkDetail.edit();
            edit.clear();
            edit.putString("link",mainUrl);
            edit.apply();
        }else{
            mainUrl = link;
        }
        flLoading = findViewById(R.id.fl_loading_layer);
        logoView = (ImageView)findViewById(R.id.imageview_logo);
        logoView.setOnClickListener(this);
        editTxUsername = (EditText)findViewById(R.id.et_username);
        editTxPassword = (EditText)findViewById(R.id.et_password);
        btnLogin = (Button)findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);
        rgType = (RadioGroup)findViewById(R.id.rg_login);
        rgType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.rb_manager:
                        type = 1; // 1 stands for Manager
                        break;
                    case R.id.rb_staff:
                        type = 2; // 2 stands for Staffs
                        break;
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_login:
                loginValidation();
                break;
            case R.id.imageview_logo:
                setServerLink();
                break;
        }
    }

    void login(){
        flLoading.setVisibility(View.INVISIBLE);
        Intent i = new Intent(LoginActivity.this,MainActivity.class);
        i.putExtra("usertype",type);
        startActivity(i);
        this.finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    void loginValidation(){
        flLoading.setVisibility(View.VISIBLE);
        String username = editTxUsername.getText().toString();
        String password = editTxPassword.getText().toString();
        OkHttpClient client = new OkHttpClient();
        url = mainUrl+"userControl.php?username="+username+"&password="+password+"&type="+type;

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        flLoading.setVisibility(View.INVISIBLE);
                        Toast.makeText(LoginActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();
                Log.d("response",myResponse);
                try {
                    JSONArray dataJsonArr = null;
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");

                    if(Objects.equals(message, "OK")){
                        dataJsonArr = json.getJSONArray("user");
                        int id = 0;
                        String name = "";
                        String password ="";
                        String username = "";
                        String ic_passport = "";
                        String citizenship = "";
                        String lastLogin = "";
                        String userType= "";
                        for (int i = 0; i < dataJsonArr.length(); i++) {
                            JSONObject c = dataJsonArr.getJSONObject(i);
                            // Storing each json item in variable
                            id = c.getInt("id");
                            name = c.getString("name");
                            password = c.getString("password");
                            username = c.getString("username");
                            //ic_passport = c.getString("ic_passport");
                            //citizenship = c.getString("citizenship");
                            lastLogin = c.getString("last_login");
                            userType = c.getString("user_type");
                        }
                        SharedPreferences userDetail = getSharedPreferences("userdetail", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edit = userDetail.edit();
                        edit.clear();
                        edit.putInt("user_id", id);
                        edit.putString("name",name);
                        edit.apply();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                login();
                            }
                        });
                    }else{
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                flLoading.setVisibility(View.INVISIBLE);
                                Toast.makeText(LoginActivity.this, ""+message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void setServerLink(){
        final EditText txtUrl = new EditText(this);
        final SharedPreferences linkDetail = getSharedPreferences("link",Context.MODE_PRIVATE);
        String link = linkDetail.getString("link","");
        // Set the default text to a link of the Queen
        txtUrl.setHint("http://331371af.ngrok.io/");

        new AlertDialog.Builder(this)
                .setTitle("Ngrok/Localhost Server Link")
                .setMessage("Current Link is: "+link)
                .setView(txtUrl)
                .setPositiveButton("All Set!", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String url = txtUrl.getText().toString();
                        SharedPreferences.Editor edit = linkDetail.edit();
                        edit.clear();
                        edit.putString("link",url);
                        edit.apply();
                        mainUrl = url;
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .show();
    }
}
