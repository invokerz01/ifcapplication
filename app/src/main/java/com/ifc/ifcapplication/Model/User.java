package com.ifc.ifcapplication.Model;


public class User {
    int id;
    String name;
    String ic_psprt_no;
    String citizenship;
    //String age;
    //String address;
    String lastLogin;
    String username;
    String password;
    String userType;

    public User(int id, String name, String ic_psprt_no, String citizenship, String lastLogin, String username, String password, String userType) {
        this.id = id;
        this.name = name;
        this.ic_psprt_no = ic_psprt_no;
        this.citizenship = citizenship;
        //this.age = age;
        //this.address = address;
        this.lastLogin = lastLogin;
        this.username = username;
        this.password = password;
        this.userType = userType;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getIc_psprt_no() {
        return ic_psprt_no;
    }

    public String getCitizenship() {
        return citizenship;
    }

    /*   public String getAge() {
        return age;
    }*/

    /*public String getAddress() {
        return address;
    }*/

    public String getLastLogin() {
        return lastLogin;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getUserType() {
        return userType;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIc_psprt_no(String ic_psprt_no) {
        this.ic_psprt_no = ic_psprt_no;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    /* public void setAge(String age) {
        this.age = age;
    }*/

    /*public void setAddress(String address) {
        this.address = address;
    }*/

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
