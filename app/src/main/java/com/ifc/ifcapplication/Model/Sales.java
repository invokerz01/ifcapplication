package com.ifc.ifcapplication.Model;

/**
 * Created by TiNkeR-Z on 2/1/2018.
 */

public class Sales {
    String day;
    String month;
    String year;
    String date;
    double total;
    String mode;

    public Sales(String day, String month, String year, String date, double total, String mode) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.date = date;
        this.total = total;
        this.mode = mode;
    }

    public String getDay() {
        return day;
    }

    public String getMonth() {
        return month;
    }

    public String getYear() {
        return year;
    }

    public String getDate() {
        return date;
    }

    public double getTotal() {
        return total;
    }

    public String getMode() {
        return mode;
    }
}
