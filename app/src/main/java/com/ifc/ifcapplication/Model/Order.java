package com.ifc.ifcapplication.Model;


public class Order
{
    private int id;
    private int menu_id;
    private int table_id;
    private int quantity;
    private double subtotal;

    public Order(int id, int menu_id, int table_id, int quantity, double subtotal) {
        this.id = id;
        this.menu_id = menu_id;
        this.table_id = table_id;
        this.quantity = quantity;
        this.subtotal = subtotal;
    }

    public int getId() {
        return id;
    }

    public int getMenu_id() {
        return menu_id;
    }

    public int getTable_id() {
        return table_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getSubtotal() {
        return subtotal;
    }
}
