package com.ifc.ifcapplication.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.ifc.ifcapplication.Activity.LoginActivity;
import com.ifc.ifcapplication.Activity.MainActivity;
import com.ifc.ifcapplication.Adapter.MenuAdapter;
import com.ifc.ifcapplication.Model.Menu;
import com.ifc.ifcapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MenuListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MenuListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuListFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String type ="";
    ArrayList<Menu> menuList;
    ArrayAdapter<Menu> menuAdapter;
    ListView listViewMenu;
    Button btnAll,btnFood,btnDrink;

    private OnFragmentInteractionListener mListener;

    public MenuListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MenuListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MenuListFragment newInstance(String param1, String param2) {
        MenuListFragment fragment = new MenuListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String title=getArguments().getString("title");
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(title);
        View v = inflater.inflate(R.layout.fragment_menu_list, container, false);
        btnAll = (Button)v.findViewById(R.id.btn_all);
        btnFood = (Button)v.findViewById(R.id.btn_food);
        btnDrink = (Button)v.findViewById(R.id.btn_drink);
        btnAll.setOnClickListener(this);
        btnFood.setOnClickListener(this);
        btnDrink.setOnClickListener(this);
        listViewMenu = (ListView)v.findViewById(R.id.listview_menu);
        menuList = new ArrayList<>();
        getMenuList("all");
        menuAdapter = new MenuAdapter(getActivity(),menuList);
        listViewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Menu menu = menuAdapter.getItem(position);
                Toast.makeText(getActivity(), ""+menu.getName(), Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }

    @Override
    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.search_menu,menu);
        MenuItem item = menu.findItem(R.id.menuSearch);
        MenuItem itemAdd = menu.findItem(R.id.action_add);
        itemAdd.setVisible(false);
        SearchView searchView = (SearchView)item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                menuAdapter.getFilter().filter(newText);
                return false;
            }
        });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_all:
                type ="all";
                getMenuList(type);
                break;
            case R.id.btn_food:
                type ="food";
                getMenuList(type);
                break;
            case R.id.btn_drink:
                type ="drink";
                getMenuList(type);
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    void getMenuList(String type){
        Toast.makeText(getActivity(), "Now Listing "+type+" type in menu", Toast.LENGTH_SHORT).show();
        menuList.clear();
        OkHttpClient client = new OkHttpClient();
        String url = LoginActivity.mainUrl+"getMenuList.php?type="+type;

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONArray dataJsonArr = null;
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");


                        dataJsonArr = json.getJSONArray("menu");
                        int id = 0;
                        String name = "";
                        double price = 0.0;
                        String type = "";
                        for (int i = 0; i < dataJsonArr.length(); i++) {
                            JSONObject c = dataJsonArr.getJSONObject(i);
                            // Storing each json item in variable
                            id = c.getInt("id");
                            name = c.getString("name");
                            price = c.getDouble("price");
                            type = c.getString("type");
                            menuList.add(new Menu(id,name,price,type));
                        }
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                listViewMenu.setAdapter(menuAdapter);
                                menuAdapter.notifyDataSetChanged();
                            }
                        });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
