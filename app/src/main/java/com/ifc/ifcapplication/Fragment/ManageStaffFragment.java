package com.ifc.ifcapplication.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.ifc.ifcapplication.Activity.LoginActivity;
import com.ifc.ifcapplication.Activity.MainActivity;
import com.ifc.ifcapplication.Adapter.StaffAdapter;
import com.ifc.ifcapplication.Model.User;
import com.ifc.ifcapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ManageStaffFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ManageStaffFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ManageStaffFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ListView lvStaff;
    ArrayList<User> staffList;
    ArrayAdapter<User> staffAdapter;
    String name,username,password,cpassword,icPassport,citizenship;
    User staff;
    AlertDialog alertEdit,alertAdd;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ManageStaffFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ManageStaffFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ManageStaffFragment newInstance(String param1, String param2) {
        ManageStaffFragment fragment = new ManageStaffFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String title=getArguments().getString("title");
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(title);
        View v =  inflater.inflate(R.layout.fragment_manage_staff, container, false);
        lvStaff = (ListView) v.findViewById(R.id.lv_staff);
        staffList = new ArrayList<>();
        getStaffList();
        staffAdapter = new StaffAdapter(getActivity(),staffList);
        lvStaff.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                staff = staffList.get(position);
                editStaffDialog(staff);
            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_table,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_add:
                addStaffDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    void addStaffDialog(){
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View addStaffView = inflater.inflate(R.layout.staff_editor_view, null);
        final EditText etname = (EditText) addStaffView.findViewById(R.id.tv_name);
        final EditText etusername = (EditText) addStaffView.findViewById(R.id.tv_username);
        final EditText etpassword = (EditText) addStaffView.findViewById(R.id.tv_password);
        final EditText etcpassword = (EditText) addStaffView.findViewById(R.id.tv_cpassword);
        final EditText etIcPassport = (EditText) addStaffView.findViewById(R.id.tv_ic_passport);
        final Spinner spinner = (Spinner)addStaffView.findViewById(R.id.spn_citizenship);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.citizenship, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        Button btnSubmit = (Button) addStaffView.findViewById(R.id.btn_submit);
        Button btnSave = (Button) addStaffView.findViewById(R.id.btn_save);
        btnSave.setVisibility(View.INVISIBLE);
        btnSubmit.setOnClickListener(this);
        Button btnCancel = (Button) addStaffView.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        Button btnDelete = (Button) addStaffView.findViewById(R.id.btn_delete);
        btnDelete.setVisibility(View.INVISIBLE);
        alertAdd = new AlertDialog.Builder(getActivity())
                .setTitle("Add New Staff")
                .setView(addStaffView)
                .show();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertAdd.dismiss();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            public void onClick(View v) {
                if(etname.length() == 0 || etusername.length() == 0 || etpassword.length() == 0 || etcpassword.length() == 0 || etIcPassport.length() == 0 ){
                    Toast.makeText(getActivity(), "Do not leave field empty", Toast.LENGTH_SHORT).show();
                }
                else if(!Objects.equals(etpassword.getText().toString(), etcpassword.getText().toString())){
                    Toast.makeText(getActivity(), "Password does not match!", Toast.LENGTH_SHORT).show();
                }else{
                    name = etname.getText().toString();
                    username = etusername.getText().toString();
                    password = etpassword.getText().toString();
                    cpassword = etcpassword.getText().toString();
                    icPassport = etIcPassport.getText().toString();
                    citizenship = spinner.getSelectedItem().toString();
                    addStaff();
                }

            }
        });

    }

    void editStaffDialog(final User staff){
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View addStaffView = inflater.inflate(R.layout.staff_editor_view, null);
        final EditText etname = (EditText) addStaffView.findViewById(R.id.tv_name);
        final EditText etusername = (EditText) addStaffView.findViewById(R.id.tv_username);
        final EditText etpassword = (EditText) addStaffView.findViewById(R.id.tv_password);
        EditText etcpassword = (EditText) addStaffView.findViewById(R.id.tv_cpassword);
        TextInputLayout cpasswordLayout = (TextInputLayout)addStaffView.findViewById(R.id.etConfirmPasswordLayout);
        cpasswordLayout.setVisibility(View.GONE);
        final EditText etIcPassport = (EditText) addStaffView.findViewById(R.id.tv_ic_passport);
        final Spinner spinner = (Spinner)addStaffView.findViewById(R.id.spn_citizenship);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.citizenship, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        Button btnDelete = (Button) addStaffView.findViewById(R.id.btn_delete);
        btnDelete.setOnClickListener(this);
        Button btnSubmit = (Button) addStaffView.findViewById(R.id.btn_submit);
        btnSubmit.setVisibility(View.INVISIBLE);
        Button btnSave = (Button) addStaffView.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);
        Button btnCancel = (Button) addStaffView.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        etname.setText(staff.getName());
        etusername.setText(staff.getUsername());
        etpassword.setText(staff.getPassword());
        etIcPassport.setText(staff.getIc_psprt_no());
        if (staff.getCitizenship() != null) {
            int spinnerPosition = adapter.getPosition(staff.getCitizenship());
            spinner.setSelection(spinnerPosition);
        }

        alertEdit = new AlertDialog.Builder(getActivity())
                .setTitle("Edit Staff")
                .setView(addStaffView)
                .show();
        btnDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // I want the dialog to close at this point
                deleteStaff(staff);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // I want the dialog to close at this point
                alertEdit.dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // I want the dialog to close at this point
                if(etname.length() == 0 || etusername.length() == 0 || etpassword.length() == 0 || etIcPassport.length() == 0 ){
                    Toast.makeText(getActivity(), "Do not leave field empty", Toast.LENGTH_SHORT).show();
                }else{
                    staff.setName(etname.getText().toString());
                    staff.setUsername(etusername.getText().toString());
                    staff.setPassword(etpassword.getText().toString());
                    staff.setIc_psprt_no(etIcPassport.getText().toString());
                    staff.setCitizenship(spinner.getSelectedItem().toString());
                    editStaff(staff);
                }
            }
        });
    }

    void addStaff(){
        String url = LoginActivity.mainUrl+"addStaff.php?name="+name+"&username="+username+"&password="+password+"&cpassword="+cpassword+"&ic_passport="+icPassport+"&citizenship="+citizenship;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();
                Log.d("Staff:",myResponse);
                try {
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");
                    if(Objects.equals(message, "OK")){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), "Staff has been added", Toast.LENGTH_SHORT).show();
                                getStaffList();
                                alertAdd.dismiss();
                            }
                        });
                    }else{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), ""+message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void editStaff(User staff){
        String url = LoginActivity.mainUrl+"editStaff.php?name="+staff.getName()+"&username="+staff.getUsername()+"&password="+staff.getPassword()+"&ic_passport="+staff.getIc_psprt_no()+"&citizenship="+staff.getCitizenship()+"&staff_id="+staff.getId();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");
                    if(Objects.equals(message, "OK")){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), "Staff data has been saved", Toast.LENGTH_SHORT).show();
                                getStaffList();
                                staffAdapter.notifyDataSetChanged();
                                alertEdit.dismiss();
                            }
                        });
                    }else{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), ""+message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void deleteStaff(User staff){
        String url = LoginActivity.mainUrl+"deleteStaff.php?staff_id="+staff.getId();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");
                    if(Objects.equals(message, "OK")){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), "Staff has been deleted", Toast.LENGTH_SHORT).show();
                                getStaffList();
                                staffAdapter.notifyDataSetChanged();
                                alertEdit.dismiss();
                            }
                        });
                    }else{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), ""+message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void getStaffList(){
        staffList.clear();
        String url = LoginActivity.mainUrl+"getStaff.php";
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONArray dataJsonArr = null;
                    JSONArray menuDetailsJsonArr = null;
                    JSONObject json = new JSONObject(myResponse);
                    // Category Basic Values
                    final String message = json.getString("message");
                    dataJsonArr = json.getJSONArray("staff");
                    int id = 0;
                    String name = "";
                    String password ="";
                    String username = "";
                    String ic_passport = "";
                    String citizenship = "";
                    String lastLogin = "";
                    String userType= "";
                    if(Objects.equals(message, "OK")){
                        for (int i = 0; i < dataJsonArr.length(); i++) {
                            JSONObject c = dataJsonArr.getJSONObject(i);
                            // Storing each json item in variable
                            id = c.getInt("id");
                            name = c.getString("name");
                            password = c.getString("password");
                            username = c.getString("username");
                            ic_passport = c.getString("ic_passport");
                            citizenship = c.getString("citizenship");
                            lastLogin = c.getString("last_login");
                            userType = c.getString("user_type");
                            staffList.add(new User(id, name, ic_passport, citizenship, lastLogin, username, password, userType));
                        }
                    }else{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), ""+message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                           lvStaff.setAdapter(staffAdapter);
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
