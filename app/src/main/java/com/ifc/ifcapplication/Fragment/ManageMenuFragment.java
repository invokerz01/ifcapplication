package com.ifc.ifcapplication.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.ifc.ifcapplication.Activity.LoginActivity;
import com.ifc.ifcapplication.Activity.MainActivity;
import com.ifc.ifcapplication.Adapter.MenuAdapter;
import com.ifc.ifcapplication.Model.Menu;
import com.ifc.ifcapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ManageMenuFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ManageMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ManageMenuFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ArrayList<Menu> menuList;
    ArrayAdapter<Menu> menuAdapter;
    String type ="";
    ListView listViewMenu;
    AlertDialog alertAdd,alertEdit;
    Menu menu;
    NumberFormat formatter = new DecimalFormat("#0.00");
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ManageMenuFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ManageMenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ManageMenuFragment newInstance(String param1, String param2) {
        ManageMenuFragment fragment = new ManageMenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String title=getArguments().getString("title");
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(title);
        View v = inflater.inflate(R.layout.fragment_manage_menu, container, false);
        listViewMenu = (ListView)v.findViewById(R.id.listview_menu);
        menuList = new ArrayList<>();
        getMenuList("all");
        menuAdapter = new MenuAdapter(getActivity(),menuList);
        listViewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Menu menu = menuList.get(position);
                editMenuDialog(menu);
            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onCreateOptionsMenu(android.view.Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.menuSearch);
        SearchView searchView = (SearchView) item.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                menuAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_add:
                addMenuDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void getMenuList(String type){
        menuList.clear();
        OkHttpClient client = new OkHttpClient();
        String url = LoginActivity.mainUrl+"getMenuList.php?type="+type;

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONArray dataJsonArr = null;
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");

                    dataJsonArr = json.getJSONArray("menu");
                    int id = 0;
                    String name = "";
                    double price = 0.0;
                    String type = "";
                    for (int i = 0; i < dataJsonArr.length(); i++) {
                        JSONObject c = dataJsonArr.getJSONObject(i);
                        // Storing each json item in variable
                        id = c.getInt("id");
                        name = c.getString("name");
                        price = c.getDouble("price");
                        type = c.getString("type");
                        menuList.add(new Menu(id,name,price,type));
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listViewMenu.setAdapter(menuAdapter);
                            menuAdapter.notifyDataSetChanged();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void addMenuDialog(){
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View addMenuView = inflater.inflate(R.layout.menu_editor_view, null);
        final EditText etMenuName = (EditText) addMenuView.findViewById(R.id.et_menu_name);
        final EditText etMenuPrice = (EditText) addMenuView.findViewById(R.id.et_menu_price);
        final Spinner spinner = (Spinner)addMenuView.findViewById(R.id.spn_food_type);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        Button btnCancel = (Button) addMenuView.findViewById(R.id.btn_cancel);
        Button btnSave = (Button) addMenuView.findViewById(R.id.btn_save);
        Button btnDelete = (Button) addMenuView.findViewById(R.id.btn_delete);
        Button btnSubmit = (Button) addMenuView.findViewById(R.id.btn_submit);
        btnDelete.setVisibility(View.INVISIBLE);
        btnSave.setVisibility(View.INVISIBLE);
        alertAdd = new AlertDialog.Builder(getActivity())
                .setTitle("Add Menu")
                .setView(addMenuView)
                .show();
        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // I want the dialog to close at this point
                alertAdd.dismiss();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // I want the dialog to close at this point

                if(etMenuName.getText().length() == 0 || etMenuPrice.getText().length() == 0){
                    Toast.makeText(getActivity(), "Please fill all field", Toast.LENGTH_SHORT).show();
                }else{
                    try{
                        String type = spinner.getSelectedItem().toString();
                        String name = etMenuName.getText().toString();
                        Double price = Double.parseDouble(etMenuPrice.getText().toString());
                        addMenu(name,price,type);
                    }catch (Exception e){
                        Toast.makeText(getActivity(), "Please enter proper price", Toast.LENGTH_SHORT).show();
                    }


                }


            }
        });
    }

    void editMenuDialog(final Menu menu){
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View addMenuView = inflater.inflate(R.layout.menu_editor_view, null);
        final EditText etMenuName = (EditText) addMenuView.findViewById(R.id.et_menu_name);
        final EditText etMenuPrice = (EditText) addMenuView.findViewById(R.id.et_menu_price);
        etMenuName.setText(menu.getName());
        etMenuPrice.setText(formatter.format(menu.getPrice()));
        final Spinner spinner = (Spinner)addMenuView.findViewById(R.id.spn_food_type);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        if (menu.getType() != null) {
            int spinnerPosition = adapter.getPosition(menu.getType());
            spinner.setSelection(spinnerPosition);
        }
        Button btnCancel = (Button) addMenuView.findViewById(R.id.btn_cancel);
        Button btnSave = (Button) addMenuView.findViewById(R.id.btn_save);
        Button btnDelete = (Button) addMenuView.findViewById(R.id.btn_delete);
        Button btnSubmit = (Button) addMenuView.findViewById(R.id.btn_submit);
        btnSubmit.setVisibility(View.INVISIBLE);
        alertEdit = new AlertDialog.Builder(getActivity())
                .setTitle("Edit Menu")
                .setView(addMenuView)
                .show();
        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertEdit.dismiss();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               deleteMenu(menu);
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if(etMenuName.getText().length() == 0 || etMenuPrice.getText().length() == 0){
                    Toast.makeText(getActivity(), "Please fill all field", Toast.LENGTH_SHORT).show();
                }else{
                    try{
                        String type = spinner.getSelectedItem().toString();
                        String name = etMenuName.getText().toString();
                        Double price = Double.parseDouble(etMenuPrice.getText().toString());
                        menu.setName(name);
                        menu.setPrice(price);
                        menu.setType(type);
                        editMenu(menu);
                    }catch (Exception e){
                        Toast.makeText(getActivity(), "Please enter proper price", Toast.LENGTH_SHORT).show();
                    }


                }


            }
        });
    }
    void addMenu(String name,Double price,String type){
        String url = LoginActivity.mainUrl+"addMenu.php?name="+name+"&price="+price+"&type="+type;
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");
                    if(Objects.equals(message, "OK")){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), "Menu has been added", Toast.LENGTH_SHORT).show();
                                getMenuList("all");
                                menuAdapter.notifyDataSetChanged();
                                alertAdd.dismiss();
                            }
                        });
                    }else{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), ""+message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void editMenu(Menu menu){
        String url = LoginActivity.mainUrl+"editMenu.php?name="+menu.getName()+"&price="+menu.getPrice()+"&type="+menu.getType()+"&menu_id="+menu.getId();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");
                    if(Objects.equals(message, "OK")){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), "Menu data has been changed", Toast.LENGTH_SHORT).show();
                                getMenuList("all");
                                menuAdapter.notifyDataSetChanged();
                                alertEdit.dismiss();
                            }
                        });
                    }else{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), ""+message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void deleteMenu(Menu menu){
        String url = LoginActivity.mainUrl+"deleteMenu.php?menu_id="+menu.getId();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");
                    if(Objects.equals(message, "OK")){
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), "Selected menu has been deleted", Toast.LENGTH_SHORT).show();
                                getMenuList("all");
                                menuAdapter.notifyDataSetChanged();
                                alertEdit.dismiss();
                            }
                        });
                    }else{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), ""+message, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
