package com.ifc.ifcapplication.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.ifc.ifcapplication.Activity.LoginActivity;
import com.ifc.ifcapplication.Activity.MainActivity;
import com.ifc.ifcapplication.Adapter.SalesAdapter;
import com.ifc.ifcapplication.Model.Sales;
import com.ifc.ifcapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SalesReportFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SalesReportFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SalesReportFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ListView lvSales;
    ArrayAdapter<Sales> salesAdapter;
    ArrayList<Sales> salesList;
    Spinner spinner;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public SalesReportFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SalesReportFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SalesReportFragment newInstance(String param1, String param2) {
        SalesReportFragment fragment = new SalesReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String title=getArguments().getString("title");
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(title);
        View v =  inflater.inflate(R.layout.fragment_sales_report, container, false);
        lvSales = (ListView)v.findViewById(R.id.lv_sales);
        spinner = (Spinner)v.findViewById(R.id.spn_mode);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.sales_mode, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        salesList = new ArrayList<>();
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                getSalesList();
                salesAdapter = new SalesAdapter(getActivity(),salesList);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    void getSalesList(){
        salesList.clear();
        String mode = spinner.getSelectedItem().toString();
        OkHttpClient client = new OkHttpClient();
        String url = LoginActivity.mainUrl+"salesReport.php?mode="+mode;

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                call.cancel();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String myResponse = response.body().string();

                try {
                    JSONArray dataJsonArr = null;
                    JSONObject json = new JSONObject(myResponse);
                    final String message = json.getString("message");

                    String day = "";
                    String month = "";
                    String year = "";
                    String date = "";
                    double total = 0.0;
                    String mode = "";
                    if(Objects.equals(message, "OK")){
                        dataJsonArr = json.getJSONArray("data");
                        mode = json.getString("mode");
                        for (int i = 0; i < dataJsonArr.length(); i++) {
                            JSONObject c = dataJsonArr.getJSONObject(i);
                            switch(mode){
                                case "daily":
                                    day = c.getString("name");
                                    date = c.getString("date");
                                    break;
                                case "monthly":
                                    month = c.getString("name");
                                    break;
                                case "yearly":
                                    year = c.getString("name");
                                    break;
                            }
                            total = c.getDouble("total");
                            salesList.add(new Sales(day, month, year, date, total, mode));
                        }
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            lvSales.setAdapter(salesAdapter);
                            salesAdapter.notifyDataSetChanged();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
